const SyncPromiseTest = require("./syncPromise");

describe("SyncPromise", () => {
  it("базовый кейс", () => {
    const p1 = new SyncPromiseTest(resolve => resolve(5))
      .then(v => v + 3)
      .then(v => v + 1);
    const p2 = p1.then(v => v + 1);

    p1.then(nine => {
      expect(nine).toBe(9);
    });
    p2.then(ten => {
      expect(ten).toBe(10);
    });
  });
});

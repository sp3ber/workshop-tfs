/*
* Реализуйте упрощенную версию Promise, имеющую только два состояния - завершен и НЕ завершен.
* Подробнее см. тесты
*
* */

const PROMISE_STATES = {
  PENDING: "PENDING",
  FULLFILLED: "FULLFILLED"
};

const isPromise = test =>
    test !== null && typeof test === "object" && "then" in test;

class SimplePromise {
  constructor(fn) {
    this._result = null;
    this._successCallbacks = [];
    this.setState(PROMISE_STATES.PENDING);
    fn(this._resolve.bind(this));
  }

  setState(state) {
  }

  _resolve(result) {
    this._result = result;
    this.setState(PROMISE_STATES.FULLFILLED);
  }
}

module.exports = SimplePromise;
